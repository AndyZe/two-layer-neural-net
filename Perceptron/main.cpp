#pragma once
#include "CommonIncludes.h"
#include <time.h>
#include "HiddenNeuron.h"
#include "OutputNeuron.h"
using namespace NNet;

namespace NNet
{
	double InputArray [NUM_OBSERVATIONS][NUM_INPUTS];
	double OutputArray[NUM_OBSERVATIONS][NUM_OUTPUTS];
	double Answers[NUM_OBSERVATIONS][NUM_OUTPUTS];
	double SSE=0;
	double ActivationArray[NUM_OBSERVATIONS][NUM_HIDDEN];
	double ErrorArray[NUM_OBSERVATIONS][NUM_OUTPUTS];
}

int main ()
{
	//Seed the random number generator
	srand( (unsigned int) time(NULL) );

	//Form input Vector
	FillInputArray();

	//Create a vector of Neuron objects to represent the hidden layer
	CreateHiddenLayer();
	system("pause");

	CreateOutputLayer();
	system("pause");

	//Read the "answers" that will be compared against
	ReadAnswers();

	//Iterate
	for (int i=0; i<NUM_ITERATIONS; i++)
	{
		SSE=0;

		for (int j=0; j<NUM_OBSERVATIONS; j++)
		{
			//Forward propagate
			FwdProp( j );

			//Calculate the error
			ComputeError( j );

			//Back propagate
			BackProp( j );
			cout<<endl<< endl;
		}

		cout<< "SSE:" << SSE<< endl;

		//Pause every X epochs
		//if (i%100==0)
			//system("Pause");
	}

	//Output the final estimated values
	SaveEstimates();

	return 0;
}