// User inputs the # of neurons in hidden layer and an object is created for each.
//Also sets up initial, random weights and biases

#pragma once
#include "CommonIncludes.h"
using namespace NNet;

vector<HiddenNeuron> HiddenLayerVector(NUM_HIDDEN);

int CreateHiddenLayer( )
{
	system("CLS");

	//Display the NN parameters
	cout<<"Number of inputs: "<< NUM_INPUTS<<endl;
	cout<<"Numer of hidden neurons: "<<NUM_HIDDEN<<endl;
	cout<<"Number of outputs: "<< NUM_OUTPUTS<<endl<<endl;

	cout<<"Hidden Layer Features"<< endl<< "-----------------------------------------"<<endl;
	//Display the Hidden Layer features
	for (int i=0; i<NUM_HIDDEN; i++)
	{
		cout<< "HIDDEN NEURON "<< i<< endl;
		HiddenLayerVector.at(i)=( HiddenNeuron() );
		HiddenLayerVector[i].GetInfo();
	}

	//HiddenLayerVector.at(0)= HiddenNeuron(-0.48, -0.27);
	//HiddenLayerVector[0].GetInfo();
	//HiddenLayerVector.at(1)= HiddenNeuron(-0.13, -0.41);
	//HiddenLayerVector[1].GetInfo();

	return 0;
}