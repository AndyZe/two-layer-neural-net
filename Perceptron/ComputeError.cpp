//Compute the error vector for each output neuron over all dimensions

#pragma once
#include "CommonIncludes.h"
using namespace NNet;

int ComputeError(int observation)
{
	//cout<<endl<< "Computing the Error." <<endl;

	//Zero out the error vector
	for (int i=0; i<NUM_OUTPUTS; i++)
	{
		ErrorArray[observation][i]=0;
	}


	//For every column, or every dimension of "answers.txt"
	for (int j=0; j<NUM_OUTPUTS; j++)
	{
		//cout<<"Answers[i][j]="<< Answers[i][j]<<endl;
		//cout<<"Output Array[i][j]="<< OutputArray[i][j]<<endl;
		ErrorArray[observation][j] += Answers[observation][j]-OutputArray[observation][j];

		SSE+= 0.5*( Answers[observation][j]-OutputArray[observation][j] )*( Answers[observation][j]-OutputArray[observation][j] );
	}


	//Display the errors
	for (int j=0; j<NUM_OUTPUTS; j++)
	{
		//cout<<"Error["<<j<<"] is "<< ErrorVector.at(j)<<endl;
	}
	//system("Pause");

	return 0;
}