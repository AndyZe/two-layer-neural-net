//Read the data that will be fitted from a txt file

#pragma once
#include "CommonIncludes.h"
using namespace NNet;

int FillInputArray()
{
	ifstream data;
	data.open("input_data.txt");

	if (data.is_open())
	{
		cout<<"Reading the data that will be fit..."<< endl;

		//For every observation, or row in "input_data.txt"
		for (int i=0; i<NUM_OBSERVATIONS; i++)
		{
			cout<< i;

			//For every column, or every input dimension of "input_data.txt"
			for (int j=0; j<NUM_INPUTS; j++)
			{
				data >> InputArray[i][j];
				cout<<"  "<< InputArray[i][j];
			}

			cout<< endl;
		}
	}
	data.close();
	
	//cout<<"Here is the first row of inputs. Check them now."<<endl;

	//for (int i=0; i<NUM_INPUTS; i++)
	//	cout<<InputArray[0][i]<<"   ";
	//cout<<endl<<endl;
	system("Pause");

	return 0;
}