#pragma once
#include "CommonIncludes.h"

class HiddenNeuron
{
	//Allocate space for up to 100 neurons in the hidden layer
	double w[100];

	//Bias
	double b;

	//The internal argument of the neuron. Bias+weighted inputs. Used for backprop
	double zeta;

	//The activation output from the neuron
	double activation;

public:
	//Constructors
	HiddenNeuron();
	HiddenNeuron(double bias, double weight );

	//int Neuron::GetInfo();
	int HiddenNeuron::GetInfo( );

	//Calculate the output of the neuron given the inputs
	double HiddenNeuron:: FProp( double InputArray[] );

	//Calculate the steepest descent and adjust the weights accordingly
	int HiddenNeuron:: BackProp(int NeuronNumber, int observation);
};