// User inputs the # of neurons in hidden layer and an object is created for each.
//Also sets up initial, random weights and biases

#pragma once
#include "CommonIncludes.h"
using namespace NNet;

vector<OutputNeuron> OutputLayerVector(NUM_OUTPUTS);

int CreateOutputLayer( )
{
	//Display the Hidden Layer features
	cout<<endl<<"Output Layer Features"<< endl<< "-----------------------------------------"<<endl;
	for (int i=0; i<NUM_OUTPUTS; i++)
	{
		cout<< "OUTPUT NEURON "<< i<< endl;
		OutputLayerVector.at(i)=( OutputNeuron() );
		OutputLayerVector[i].GetInfo();
	}

	double weights[2]= {.09, -0.17};

	//OutputLayerVector.at(0)=( OutputNeuron( 0.48, weights ) );
	//OutputLayerVector[0].GetInfo();

	return 0;
}