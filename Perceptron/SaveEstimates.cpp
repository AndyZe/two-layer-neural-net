//Save the final estimates to a .txt file

#pragma once
#include "CommonIncludes.h"
using namespace NNet;

int SaveEstimates()
{
	ofstream stream;
	stream.open("FinalEstimates.txt");

	if (stream.is_open())
	{
		//For every observation, or row in "input_data.txt"
		for (int i=0; i<NUM_OBSERVATIONS; i++)
		{
			//For the # of outputs per observation
			for (int j=0; j<NUM_OUTPUTS; j++)
			{
				stream<< OutputArray[i][j] <<"\t";
			}
			stream<< endl;
		}
	}

	stream.close();
	cout<<"The final estimates have been saved."<< endl;

	return 0;
}