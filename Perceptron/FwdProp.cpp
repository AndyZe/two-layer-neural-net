//Go through and sum bias+weighted inputs, then calculate the activation fxn
//for each neuron

#pragma once
#include "CommonIncludes.h"
using namespace NNet;

extern vector<HiddenNeuron> HiddenLayerVector;
extern vector<OutputNeuron> OutputLayerVector;

int FwdProp(int observation)
{
	//cout<< endl<< "Activation function outputs: "<< endl;
	//For each neuron in hidden layer
	for (int i=0; i<NUM_HIDDEN; i++)
	{
		//Feed the j-th row of data into Hidden Neuron i
		ActivationArray[observation][i] = HiddenLayerVector.at(i).FProp(InputArray[observation]);
		//cout<< "Hidden Neuron "<< i<< " Activation Output: "<< ActivationArray[observation][i]<< endl;
	}

	//Now run the activation function outputs through the summing output neurons
	for (int k=0; k<NUM_OUTPUTS; k++)
	{	
		OutputArray[observation][k]= OutputLayerVector.at(k).FProp( ActivationArray[observation] );
		//cout<< "Neural network output "<<j<<","<<k<<"= "<< OutputArray[j][k]<<endl;
	}


	return 0;
}