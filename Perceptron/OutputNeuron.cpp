#include "CommonIncludes.h"
#include <math.h>
using namespace NNet;

extern vector<double> ErrorVector;

//Constructors
OutputNeuron::OutputNeuron()
{
	b= rand()/(1.0*RAND_MAX)-0.5;

	//Fill the weights vector with random values between -0.5 and 0.5
	for (int i=0; i< NUM_HIDDEN; i++)
	{
		w[i]= rand()/(1.0*RAND_MAX)-0.5;
	}

}

OutputNeuron::OutputNeuron(double bias, double weights[NUM_HIDDEN])
{
	b= bias;
	w[0]= weights[0];
	w[1]= weights[1];
}

//Display neuron info
int OutputNeuron::GetInfo()
{
	cout<<"Bias: "<< b<< endl;
	cout<<"Weights:"<< endl;
	for (int i=0; i< NUM_HIDDEN; i++)
	{
		cout<< w[i]<< endl;
	}
	cout<<endl;

	return 0;
}

//Compute the linear ouput for this SINGLE neuron, SINGLE observation
double OutputNeuron::FProp( double InputArray[] )
{
	//Used to compute the sum. Initialize it to the neuron's bias
	//cout<<"b: "<<b<<endl;
	double sum= b;

	//Sum the weighted inputs from the hidden neurons + the bias
	for (int i=0; i< NUM_HIDDEN; i++ )
	{
		sum+= InputArray[i]*w[i];
	}
	//cout<< "Sum: "<< sum<< endl;

	return sum;
}

//Calculate the steepest descent and adjust the weights accordingly
int OutputNeuron:: BackProp(int NeuronNumber, int observation)
{
	//First adjust the bias
	b+= -OUTPUT_BIAS_LEARNING_RATE*-2*ErrorArray[observation][NeuronNumber];
	cout<<"New output bias: "<<b<<endl;

	//Now adjust the weights
	cout<< "New output weights: ";
	for (int i=0; i<NUM_HIDDEN; i++)
	{
		w[i]+= -OUTPUT_WT_LEARNING_RATE* -2*ErrorArray[observation][NeuronNumber] *ActivationArray[observation][i];
		cout<<w[i]<<"\t";
	}
	cout<<endl<<endl;

	return 0;
}

//Access the weights of this neuron
double OutputNeuron::GetWeight (int position)
{
	return w[position];
}