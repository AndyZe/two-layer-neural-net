//Adjust the weights and biases

#pragma once
#include "CommonIncludes.h"
using namespace NNet;

extern vector<HiddenNeuron> HiddenLayerVector;
extern vector<OutputNeuron> OutputLayerVector;

int BackProp( int observation )
{
	//Backprop through the hidden neurons
	for (int i=0; i<NUM_HIDDEN; i++)
	{
		cout<<"HIDDEN NEURON "<<i<<" ADJUSTMENTS:"<<endl;
		HiddenLayerVector.at(i).BackProp( i,observation );
		cout<<endl<<endl;
	}


	//Backprop through the output neurons
	for (int i=0; i<NUM_OUTPUTS; i++)
	{
		//cout<<"OUTPUT NEURON "<<i<<" ADJUSTMENTS:"<<endl;
		OutputLayerVector.at(i).BackProp( i,observation );
		//cout<<endl<<endl;
	}

	return 0;
}