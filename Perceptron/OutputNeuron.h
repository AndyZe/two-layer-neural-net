#pragma once
#include "CommonIncludes.h"

class OutputNeuron
{
public:
	//Allocate space for up to 100 neurons in the hidden layer
	double w[100];

	//Bias
	double b;

	//Constructor
	OutputNeuron();
	OutputNeuron(double bias, double weights[2] );

	int OutputNeuron::GetInfo( );

	//Calculate the output of the neuron given the inputs
	double OutputNeuron:: FProp( double InputArray[] );

	//Calculate the steepest descent and adjust the weights accordingly
	int OutputNeuron:: BackProp( int NeuronNumber, int observation );

	//Access the weights of this neuron
	double OutputNeuron:: GetWeight (int position);
};