#include "CommonIncludes.h"
#include <math.h>
using namespace NNet;

extern vector<OutputNeuron> OutputLayerVector;
extern vector<double> ErrorVector;

//Constructors
HiddenNeuron::HiddenNeuron()
{
	b= rand()/(1.0*RAND_MAX)-0.5;

	//Fill the weights vector with random values between -0.5 and 0.5
	for (int i=0; i< NUM_INPUTS; i++)
	{
		w[i]= rand()/(1.0*RAND_MAX)-0.5;
	}

}

HiddenNeuron::HiddenNeuron(double bias, double weight )
{
	b= bias;
	w[0]= weight;
}

//Display neuron info
int HiddenNeuron::GetInfo()
{
	cout<<"Bias: "<< b<< endl;
	cout<<"Weights:"<< endl;
	for (int i=0; i< NUM_INPUTS; i++)
	{
		cout<< w[i]<< endl;
	}
	cout<<endl;

	return 0;
}

//Compute the activation function for this SINGLE neuron, SINGLE observation
double HiddenNeuron::FProp( double InputArray[] )
{
	////Check the Input
	//cout<<"Inputs to neuron: ";
	//for (int i=0; i< NUM_INPUTS; i++)
	//{
	//	cout<<InputArray[i]<<"  ";
	//}
	//cout<<endl;

	//Used to compute the sum. Initialize it to the neuron's bias
	//cout<<"b: "<<b<<endl;
	zeta = b;

	//Sum the weighted inputs + the bias
	for (int i=0; i< NUM_INPUTS; i++ )
	{
		zeta+= InputArray[i]*w[i];
	}
	//cout<< "Zeta: "<< zeta<< endl;

	//Apply the sigmoid activation fxn
	activation= 1/(1+exp(-zeta));

	cout<<"Activation: "<< activation<<endl;

	return activation;
}

//Calculate the steepest descent and adjust the weights accordingly
int HiddenNeuron:: BackProp(int NeuronNumber, int observation)
{
	cout<< "Activation: "<< activation<<endl;
	//Compute sigma', the derivative of the activation fxn
	double sigmaprime= activation*(1-activation);
	cout<< "Sigmaprime: "<< sigmaprime<< endl;

	//cout<<"Previous bias: "<<b<<endl<<endl;

////////   Adjust the bias   /////////////////////////////
	for (int i=0; i<NUM_OUTPUTS; i++)
	{
		b+= -HIDDEN_BIAS_LEARNING_RATE*-2*ErrorArray[observation][i]*OutputLayerVector.at(i).GetWeight(NeuronNumber)*sigmaprime;
	}
	cout<<"New hidden bias: "<<b<<endl;

///////   Adjust the weights   /////////////////////////////

	//For each weight of the neuron
	for (int i=0; i<NUM_INPUTS; i++)
	{
		//Sum up the contribution from each output path
		for (int j=0; j<NUM_OUTPUTS; j++)
		{
			w[i]+= -HIDDEN_WT_LEARNING_RATE*-2*ErrorArray[observation][i]*OutputLayerVector.at(j).GetWeight(NeuronNumber)*sigmaprime;
		}
	}

	cout<<"New hidden weights: "<< endl;
	for (int i=0; i<NUM_INPUTS; i++)
	{
		cout<< w[i]<<"\t";
	}
	cout<<endl;

	return 0;
}