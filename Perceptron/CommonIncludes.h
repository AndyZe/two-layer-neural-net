#pragma once

#include <vector>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include "HiddenNeuron.h"
#include "OutputNeuron.h"

using namespace std;

#define NUM_OBSERVATIONS 41
#define NUM_INPUTS 1
#define NUM_HIDDEN 11
#define NUM_OUTPUTS 1
#define OUTPUT_BIAS_LEARNING_RATE .05	//.01
#define OUTPUT_WT_LEARNING_RATE .05		// -.01 ?!
#define HIDDEN_BIAS_LEARNING_RATE .05	//.01
#define HIDDEN_WT_LEARNING_RATE .5	//.1 is good
#define NUM_ITERATIONS 10

namespace NNet
{
	extern double InputArray [NUM_OBSERVATIONS][NUM_INPUTS];
	extern double OutputArray[NUM_OBSERVATIONS][NUM_OUTPUTS];
	extern double Answers[NUM_OBSERVATIONS][NUM_OUTPUTS];
	extern double SSE;
	extern double ActivationArray[NUM_OBSERVATIONS][NUM_HIDDEN];
	extern double ErrorArray[NUM_OBSERVATIONS][NUM_OUTPUTS];
}

//Function definitions
int FillInputArray();
int CreateHiddenLayer( );
int CreateOutputLayer( );
int ReadAnswers();
int FwdProp( int observation );
int ComputeError(int observation);
int BackProp(int observation);
int SaveEstimates();