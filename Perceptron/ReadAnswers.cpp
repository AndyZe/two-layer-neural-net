//Read the answers that are used in error computation from a txt file

#pragma once
#include "CommonIncludes.h"
using namespace NNet;

int ReadAnswers()	
{
	ifstream answers;
	answers.open("answers.txt");

	if (answers.is_open())
	{
		//cout<<"Reading the answers that will be used to compute error..."<< endl;

		//For every observation, or row in "answers.txt"
		for (int i=0; i<NUM_OBSERVATIONS; i++)
		{
			cout<< i<< "   ";

			//For every column, or every dimension of "answers.txt"
			for (int j=0; j<NUM_OUTPUTS; j++)
			{
				answers >> Answers[i][j];
				cout<< Answers[i][j]<< "   ";
			}

			cout<< endl;
		}
	}
	answers.close();

	cout<< endl<< "Check that the proper 'Answers' were loaded now."<< endl;
	system("Pause");

	return 0;
}